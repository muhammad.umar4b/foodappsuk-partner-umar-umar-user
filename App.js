import React, {useEffect, useMemo, useState} from 'react';
import {StatusBar} from 'react-native';

import axios from 'axios';
import 'react-native-gesture-handler';

import MasterRoute from './src/navigation/MasterRoute';
import serviceListJson from './src/json/order-service.json';

import {GlobalContext} from './src/context/GlobalContext';
import {getItem} from './src/utilities/async-storage';

const App = () => {
  const [globalState, setGlobalState] = useState({
    sign_in_token: '',
    user: null,
    service_type: '',
    kitchenNotes: '',
    serviceCharge: 0,
    restaurantInfo: null,
    cartList: {
      note: '',
      customer: '',
      restaurant: '',
      count: 0,
      subTotal: 0,
      foodItems: [],
    },
    customer: '',
    mobileNo: '',
    postCode: '',
    serviceListArea: serviceListJson,
    restaurantId: null,
    restaurantUniId: null,
    restaurantPostCode: null,
  });
  console.log('globalState App', globalState);

  const value = useMemo(() => {
    return {globalState, setGlobalState};
  }, [globalState, setGlobalState]);

  useEffect(() => {
    const updateGlobalState = {...globalState};
    getItem('sign_in_token').then(res => {
      if (res) {
        updateGlobalState.sign_in_token = res;
        getItem('user').then(res => {
          if (res) {
            updateGlobalState.user = JSON.parse(res);
            setGlobalState(updateGlobalState);
          }
        });
        getItem('customer').then(res => {
          if (res) {
            updateGlobalState.customer = res;
            setGlobalState(updateGlobalState);
          }
        });
      }
    });
  }, []);

  useEffect(() => {
    if (globalState.sign_in_token) {
      axios.interceptors.request.use(
        function (config) {
          config.headers.common['x-auth-token'] = globalState.sign_in_token;
          return config;
        },
        function (error) {
          return Promise.reject(error);
        },
      );
    }
  }, [globalState.sign_in_token]);

  // useEffect(() => {
  //   console.log('----> ', globalState.cartList.foodItems);
  // }, [globalState.cartList.length]);

  return (
    <GlobalContext.Provider value={value}>
      <StatusBar barStyle="light-content" backgroundColor={'#D2181B'} />
      <MasterRoute />
    </GlobalContext.Provider>
  );
};

export default App;

// export default AppRegistry.registerComponent('Appname', () => App) // For IOS
