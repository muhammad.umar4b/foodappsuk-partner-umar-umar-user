import React from 'react'
import { Text, Pressable, View } from 'react-native'

import AntDesign from 'react-native-vector-icons/AntDesign'

import styles from './styles'

export default function CartItem(props) {
  const { item, index, addToCart, removeFromCart } = props

  const {
    contentArea,
    contentTitle,
    contentPrice,
    addedListArea,
    addButtonArea,
    addButtonAreaText,
    addedListAreaContent,
    addButtonAreaPlusIcon,
    addButtonAreaMinusIcon,
  } = styles

  const { quantity, name, options, price } = item

  return (
    <View style={addedListArea}>
      <View style={addedListAreaContent}>
        <View style={addButtonArea}>
          <Pressable onPress={() => addToCart(index)}>
            <AntDesign
              style={addButtonAreaPlusIcon}
              name='plus'
              size={18}
              color='black'
            />
          </Pressable>
          <Text style={addButtonAreaText}>{quantity}</Text>
          <Pressable onPress={() => removeFromCart(index)}>
            <AntDesign
              style={addButtonAreaMinusIcon}
              name='minus'
              size={18}
              color='black'
            />
          </Pressable>
        </View>

        <View style={contentArea}>
          <Text style={contentTitle}>{name}</Text>

          {options.length > 0 && (
            <View>
              {options.map((option, key) => (
                <Text key={key} style={key > 0}>
                  {option}
                  {key + 1 < options.length ? ',' : ''}
                </Text>
              ))}
            </View>
          )}
        </View>
      </View>
      <View>
        <Text style={contentPrice}>£{price}</Text>
      </View>
    </View>
  )
}
