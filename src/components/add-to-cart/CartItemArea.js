import React, {useContext} from 'react';
import {ScrollView, Text, View} from 'react-native';

import styles from './styles';

import CartItem from './CartItem';
import {GlobalContext} from '../../context/GlobalContext';

export default function CartItemArea(props) {
  const {globalState, setGlobalState} = useContext(GlobalContext);
  const {cartList} = globalState;

  const {addToCart, removeFromCart} = props;

  const {height55, addedCartAreaHeader, addedCartAreaHeaderText} = styles;

  return (
    <View style={height55}>
      <View style={addedCartAreaHeader}>
        <Text style={addedCartAreaHeaderText}>
          {cartList ? cartList.foodItems.length : 0} items
        </Text>
      </View>
      <ScrollView>
        {cartList ? (
          cartList.foodItems.map((item, index) => (
            <CartItem
              key={index}
              index={index}
              item={item}
              addToCart={addToCart}
              removeFromCart={removeFromCart}
            />
          ))
        ) : (
          <View>
            <Text>No item found!</Text>
          </View>
        )}
      </ScrollView>
    </View>
  );
}
