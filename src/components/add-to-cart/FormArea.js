import React, {useContext} from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {GlobalContext} from '../../context/GlobalContext';

import styles from './styles';

export default function FormArea(props) {
  const {globalState, setGlobalState} = useContext(GlobalContext);
  const {cartList} = globalState;
  const {state, navigation, restaurantId, restaurantUniId} = props;

  const {
    kitchenNoteText,
    kitchenNoteInput,
    extraKitchenNote,
    kitchenNoteButton,
    kitchenNoteButtonArea,
  } = styles;

  const {subTotal} = state;

  return (
    <View>
      <ScrollView>
        <Text style={extraKitchenNote}>Extra Allergy Note</Text>
        <KeyboardAvoidingView behavior="position" style={{flex: 1}}>
          <TextInput
            value={cartList ? cartList.note : ''}
            // onChangeText={(value) => setCartList(...cartList, (note = value))}
            onChangeText={value =>
              // setGlobalState({...globalState, cartList: (note = value)})
              setGlobalState({...globalState, kitchenNotes: ''})
            }
            style={kitchenNoteInput}
            keyboardType={'default'}
            editable={!!cartList}
          />
        </KeyboardAvoidingView>
      </ScrollView>

      <View style={kitchenNoteButtonArea}>
        <TouchableOpacity
          style={[kitchenNoteButton, subTotal === 0]}
          onPress={() => {
            navigation.navigate('Order', {restaurantId, restaurantUniId});
            // console.log('Form area', restaurantId, restaurantUniId);
          }}
          disabled={subTotal === 0}>
          <Text style={kitchenNoteText}>Check out £{subTotal}</Text>
        </TouchableOpacity>
      </View>
    </View>
    // </KeyboardAvoidingView>
  );
}
