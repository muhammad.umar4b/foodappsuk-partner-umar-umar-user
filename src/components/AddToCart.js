import React, {useContext, useEffect, useLayoutEffect, useState} from 'react';
import {ScrollView} from 'react-native';
import {GlobalContext} from '../context/GlobalContext';
import CartItemArea from './add-to-cart/CartItemArea';
import FormArea from './add-to-cart/FormArea';
import {container} from './add-to-cart/styles';

const AddToCart = ({navigation, route}) => {
  const {globalState, setGlobalState} = useContext(GlobalContext);
  const {cartList} = globalState;

  const {restaurantInfo, restaurantId, restaurantUniId} = route.params;
  const [state, setState] = useState({
    subTotal: '',
  });
  // console.log('cart', cartList);
  useEffect(() => {
    if (cartList) {
      const {note, subTotal} = cartList;

      setState({note, subTotal});
      //   console.log(state)
    } else setState({note: '', subTotal: 0});
  }, [cartList]);
  useLayoutEffect(() => {
    navigation.setOptions({
      title: restaurantInfo ? restaurantInfo.name : 'Restaurant',
    });
  }, [navigation, restaurantInfo]);

  const addToCart = async index => {
    let updateCartList = {...cartList};

    updateCartList.foodItems = updateCartList.foodItems.map((item, key) => {
      if (key === index) {
        item.quantity = item.quantity + 1;
        item.price = parseFloat(
          parseFloat(
            item.basePrice * item.quantity + item.optionsPrice * item.quantity,
          ).toFixed(2),
        );
        updateCartList.subTotal = parseFloat(
          parseFloat(
            updateCartList.subTotal + item.basePrice + item.optionsPrice,
          ).toFixed(2),
        );
      }

      return item;
    });

    setGlobalState({...globalState, cartList: updateCartList});

    // setCartList(updateCartList);
    // dispatch({ type: SET_CART_LIST, payload: updateCartList });
  };

  const removeFromCart = async index => {
    let updateCartList = {...cartList};

    const cartItem = updateCartList.foodItems.find(
      (item, key) => index === key,
    );

    if (cartItem.quantity === 1) {
      updateCartList.foodItems = updateCartList.foodItems.filter(
        (item, key) => key !== index,
      );

      let subTotal = 0;
      updateCartList.foodItems.forEach(item => {
        subTotal = parseFloat(parseFloat(subTotal + item.price).toFixed(2));
      });

      updateCartList.subTotal = subTotal;
      updateCartList.count = updateCartList.foodItems.length;
    } else {
      updateCartList.foodItems = updateCartList.foodItems.map((item, key) => {
        if (key === index) {
          item.quantity = item.quantity - 1;
          item.price = parseFloat(
            parseFloat(
              item.basePrice * item.quantity +
                item.optionsPrice * item.quantity,
            ).toFixed(2),
          );
          updateCartList.subTotal = parseFloat(
            parseFloat(
              updateCartList.subTotal - (item.basePrice + item.optionsPrice),
            ).toFixed(2),
          );
        }

        return item;
      });
    }
    setGlobalState({
      ...globalState,
      cartList: updateCartList.count === 0 ? null : updateCartList,
    });
    // setCartList(updateCartList.count === 0 ? null : updateCartList);
    // dispatch({ type: SET_CART_LIST, payload: updateCartList.count === 0 ? null : updateCartList });
  };

  //   console.log(state)

  return (
    <ScrollView
      contentContainerStyle={container}
      showsVerticalScrollIndicator={false}>
      <CartItemArea addToCart={addToCart} removeFromCart={removeFromCart} />
      <FormArea
        state={state}
        restaurantInfo={restaurantInfo}
        restaurantUniId={restaurantUniId}
        restaurantId={restaurantId}
        navigation={navigation}
      />
    </ScrollView>
  );
};

export default AddToCart;
