import React, {useEffect, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {apiBaseUrl} from '../config';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import globalStyles from '../styles/globalStyles';

const Restaurant = ({navigation, route}) => {
  const [allCousines, setAllCousines] = useState('');
  const getAllCusines = async () => {
    try {
      const response = await axios.get(`${apiBaseUrl}cuisine/get-all-cuisines`);
      return response.data;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const {restaurantList, postCode} = route.params;
  useEffect(() => {
    getAllCusines();
  }, [restaurantList]);

  return (
    <SafeAreaView
      style={[globalStyles.container, globalStyles.paddingHorizontal0]}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={[globalStyles.paddingHorizontal5, globalStyles.paddingTop2]}>
          <Text style={[styles.dataHeaderText, globalStyles.textRed]}>
            {restaurantList.length} found within this {postCode}
          </Text>
        </View>
        <View>
          {restaurantList.map((item, index) => (
            <TouchableOpacity
              key={index}
              style={{
                backgroundColor: '#fff',
                margin: 5,
                padding: 5,
                paddingBottom: 10,
                paddingTop: 10,
                alignItems: 'center',
                borderRadius: 5,
                // borderBottomColor: '#b1b1b1',
                // borderBottomWidth: 5,
                // borderRightColor: '#b1b1b1',
                // borderRightWidth: 5,
                elevation: 3,
              }}
              onPress={() =>
                navigation.navigate('RestaurantDetails', {
                  restaurantId: item.restaurant.uniId,
                  restaurantUniId: item.restaurant._id,
                  restaurantPostCode: item.restaurant.postCode,
                })
              }>
              <Image
                style={styles.restaurantListAreaLeftImage}
                source={{uri: `${item.restaurant.logo}`}}
              />
              <View>
                <Text style={styles.restaurantListAreaTitle}>
                  {item.restaurant.name}
                </Text>
              </View>
              <View>
                <Text style={styles.restaurantListAreaAddress}>
                  {item.restaurant.address.address} , {item.restaurant.postCode}
                </Text>
              </View>
              <View>
                <Text style={styles.restaurantTiming}>
                  {
                    item.restaurant.restaurantTiming.restaurantTiming
                      .bookingStartTime
                  }{' '}
                  TO{' '}
                  {
                    item.restaurant.restaurantTiming.restaurantTiming
                      .bookingEndTime
                  }{' '}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  restaurantTypeArea: {
    flexDirection: 'row',
  },
  restaurantTypeAreaSingle: {
    paddingTop: hp('0.5%'),
    paddingBottom: hp('1%'),
    paddingHorizontal: wp('3'),
    marginLeft: wp('2%'),
    borderRadius: 15,
    marginVertical: hp('2%'),
  },
  restaurantTypeActive: {
    borderWidth: 1,
    borderColor: '#D2181B',
  },
  restaurantTypeImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  dataHeaderText: {
    fontSize: 20,
    fontWeight: '700',
    paddingBottom: hp('1.5%'),
  },
  restaurantListArea: {
    paddingVertical: hp('1.5%'),
    flexDirection: 'row',
    borderTopColor: '#dcd6d6',
    borderTopWidth: 1,
  },
  restaurantListAreaLeftImage: {
    width: 80,
    height: 80,
    borderRadius: 40,
    // borderColor: '#fff',
    borderWidth: 1,
    margin: 8,
    padding: 8,
  },
  restaurantListAreaRight: {
    paddingLeft: wp('5%'),
  },
  restaurantListAreaTitle: {
    fontSize: 18,
    color: '#000',
  },
  restaurantListAreaAddress: {
    fontSize: 14,
    color: '#4b5563',
    paddingTop: hp('0.5%'),
    textAlign: 'center',
  },
  restaurantTiming: {
    fontSize: 14,
    color: '#D2181B',
    paddingTop: hp('0.5%'),
    textAlign: 'center',
  },
  deliveryTimeArea: {
    flexDirection: 'row',
    paddingTop: hp('1%'),
    paddingBottom: hp('2%'),
  },
  deliveryTimeAreaLeft: {
    flexDirection: 'row',
  },
  deliveryTimeAreaText: {
    paddingTop: hp('0.2%'),
    paddingLeft: wp('1%'),
    color: '#D2181B',
  },
  deliveryTimeAreaRight: {
    flexDirection: 'row',
    paddingLeft: wp('5%'),
  },
});

export default Restaurant;
