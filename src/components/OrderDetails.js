import moment from 'moment';
import React, {useContext, useEffect, useState} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {Cell, Table, TableWrapper} from 'react-native-table-component';
import {GlobalContext} from '../context/GlobalContext';
import globalStyles from '../styles/globalStyles';

const OrderDetails = ({route}) => {
  const {item} = route.params;
  const tableHead = ['Qty', 'Description', 'Amount'];
  const [tableData, setTableData] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const {globalState} = useContext(GlobalContext);
  const {customer} = globalState;
  console.log(item, 'itemOrder');

  useEffect(() => {
    if (item) {
      let updateTableData = [];
      let totalAmount = 0;
      // item.cartList.foodItems.forEach(item => {
      const qty = parseInt(item.quantity);
      const price = parseFloat(item.price).toFixed(2);
      totalAmount += qty * price;
      const priceText = `£${(qty * price).toFixed(2)}`;
      const arr = [qty, item.title, priceText];
      updateTableData.push(arr);
      // });

      setTotalPrice(totalAmount.toFixed(2));
      setTableData(updateTableData);
    }
  }, [item]);

  return (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <View style={styles.restaurantHeader}>
            <View>
              <Text
                style={[
                  globalStyles.f18,
                  globalStyles.textCenter,
                  globalStyles.fw700,
                ]}>
                {item.restaurant.name}
              </Text>
              <Text style={globalStyles.textCenter}>
                {item.restaurant.address}
              </Text>
              <Text style={globalStyles.textCenter}>
                {moment(item.createdAt).format('DD/MM/YYYY')}
              </Text>
              <Text style={globalStyles.textCenter}>{item.orderNumber}</Text>
            </View>
          </View>
          <View style={styles.tableArea}>
            <Table>
              <TableWrapper style={styles.tableHead}>
                {tableHead.map((item, index) => (
                  <Cell
                    key={index}
                    data={item}
                    textStyle={[
                      styles.tableText,
                      index === 2 && styles.tableTextRight,
                    ]}
                  />
                ))}
              </TableWrapper>
              {tableData.map((rowData, index) => (
                <TableWrapper key={index} style={styles.tableBody}>
                  {rowData.map((cellData, cellIndex) => (
                    <Cell
                      key={cellIndex}
                      data={cellData}
                      textStyle={[
                        styles.tableText,
                        cellIndex === 2 && styles.tableTextRight,
                      ]}
                    />
                  ))}
                </TableWrapper>
              ))}
            </Table>
          </View>
          <View style={styles.priceArea}>
            <View
              style={[
                globalStyles.flexDirectionRow,
                globalStyles.justifyBetween,
              ]}>
              <Text
                style={[
                  globalStyles.fw700,
                  globalStyles.paddingLeft5,
                  globalStyles.paddingBottom1,
                ]}>
                Sub Total
              </Text>
              <Text style={[globalStyles.fw700, globalStyles.paddingRight7]}>
                £{totalPrice ? totalPrice : '0.00'}
              </Text>
            </View>
            <View
              style={[
                globalStyles.flexDirectionRow,
                globalStyles.justifyBetween,
              ]}>
              <Text
                style={[
                  globalStyles.fw700,
                  globalStyles.paddingLeft5,
                  globalStyles.paddingBottom1,
                ]}>
                Discount (-)
              </Text>
              <Text style={[globalStyles.fw700, globalStyles.paddingRight7]}>
                £{item.discount ? parseFloat(item.discount).toFixed(2) : '0.00'}
              </Text>
            </View>
            {item.orderType === 'delivery' && (
              <View
                style={[
                  globalStyles.flexDirectionRow,
                  globalStyles.justifyBetween,
                ]}>
                <Text
                  style={[
                    globalStyles.fw700,
                    globalStyles.paddingLeft5,
                    globalStyles.paddingBottom1,
                  ]}>
                  Delivery Charges (+)
                </Text>
                <Text style={[globalStyles.fw700, globalStyles.paddingRight7]}>
                  £
                  {item.deliveryCharge
                    ? parseFloat(item.deliveryCharge).toFixed(2)
                    : '0.00'}
                </Text>
              </View>
            )}
            {!item.tableNo && (
              <View
                style={[
                  globalStyles.flexDirectionRow,
                  globalStyles.justifyBetween,
                ]}>
                <Text
                  style={[
                    globalStyles.fw700,
                    globalStyles.paddingLeft5,
                    globalStyles.paddingBottom1,
                  ]}>
                  Service Charges (+)
                </Text>
                <Text style={[globalStyles.fw700, globalStyles.paddingRight7]}>
                  {item.serviceCharge
                    ? parseFloat(item.serviceCharge).toFixed(2)
                    : '0.00'}
                </Text>
              </View>
            )}
          </View>
          <View style={styles.totalPriceArea}>
            <View
              style={[
                globalStyles.flexDirectionRow,
                globalStyles.justifyBetween,
              ]}>
              <Text
                style={[
                  globalStyles.fw700,
                  globalStyles.paddingLeft5,
                  globalStyles.paddingBottom1,
                ]}>
                Total Payment ({item.paymentMethod})
              </Text>
              <Text style={[globalStyles.fw700, globalStyles.paddingRight7]}>
                £
                {item.totalPrice
                  ? parseFloat(item.totalPrice).toFixed(2)
                  : '0.00'}
              </Text>
            </View>
          </View>
          {item.orderType === 'delivery' && (
            <View style={styles.deliveryArea}>
              <Text
                style={[
                  globalStyles.fw700,
                  globalStyles.textCenter,
                  globalStyles.f18,
                ]}>
                Delivered To: {item.customerName}
              </Text>
              <Text
                style={[
                  globalStyles.fw700,
                  globalStyles.textCenter,
                  globalStyles.f18,
                ]}>
                {item.address}
              </Text>
            </View>
          )}
          {(item.orderType === 'collection' || item.orderType === 'pickup') && (
            <View style={styles.deliveryArea}>
              <Text
                style={[
                  globalStyles.fw700,
                  globalStyles.textCenter,
                  globalStyles.f18,
                ]}>
                Collection For: {item.customerName}
              </Text>
            </View>
          )}
          {item.tableNo && (
            <View style={styles.deliveryArea}>
              <Text
                style={[
                  globalStyles.fw700,
                  globalStyles.textCenter,
                  globalStyles.f18,
                ]}>
                Dine In: {item.tableNo}
              </Text>
            </View>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: hp('2%'),
    paddingHorizontal: wp('5%'),
  },
  radioButtonContainer: {
    flexDirection: 'row',
  },
  radioButtonActive: {
    height: 18,
    width: 18,
    backgroundColor: '#F8F8F8',
    borderRadius: 9,
    borderWidth: 1,
    borderColor: '#D2181B',
    alignItems: 'center',
    justifyContent: 'center',
  },
  radioButton: {
    height: 18,
    width: 18,
    backgroundColor: '#F8F8F8',
    borderRadius: 9,
    borderWidth: 1,
    borderColor: '#adadad',
    alignItems: 'center',
    justifyContent: 'center',
  },
  radioButtonIconActive: {
    height: 10,
    width: 10,
    borderRadius: 6,
    backgroundColor: '#D2181B',
  },
  radioButtonIcon: {
    height: 10,
    width: 10,
    borderRadius: 6,
    backgroundColor: '#adadad',
  },
  lineActive: {
    height: 20,
    width: 3,
    backgroundColor: '#D2181B',
    marginLeft: 7.5,
  },
  line: {
    height: 20,
    width: 3,
    backgroundColor: '#adadad',
    marginLeft: 7.5,
  },
  timelineArea: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: hp('1%'),
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
  },
  restaurantHeader: {
    // paddingTop: hp("1%"),
    paddingBottom: hp('1%'),
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
  },
  tableArea: {
    marginTop: hp('2%'),
    paddingBottom: hp('2%'),
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
  },
  tableHead: {
    flexDirection: 'row',
    height: 40,
    backgroundColor: '#dfdfe3',
  },
  tableBody: {
    flexDirection: 'row',
    backgroundColor: '#ebebef',
  },
  tableText: {
    margin: 6,
    fontWeight: '700',
    textAlign: 'center',
  },
  tableTextRight: {
    textAlign: 'right',
    paddingRight: wp('5%'),
  },
  priceArea: {
    marginTop: hp('2%'),
    paddingBottom: hp('1%'),
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
  },
  totalPriceArea: {
    marginTop: hp('1%'),
    paddingBottom: hp('1%'),
  },
  deliveryArea: {
    marginTop: hp('2%'),
  },
});

export default OrderDetails;
