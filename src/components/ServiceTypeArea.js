import React from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {color} from 'react-native-reanimated';
import styles from './styles';

export default function ServiceTypeArea(props) {
  const {serviceListArea, updateServiceType} = props;

  const {
    restaurantInfoArea,
    serviceTypeArea,
    serviceArea,
    serviceAreaSingle,
    activeService,
  } = styles;

  return (
    <View style={restaurantInfoArea}>
      <View style={serviceTypeArea}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          style={serviceArea}
          pagingEnabled={true}>
          {serviceListArea.map((item, index) => (
            <TouchableOpacity
              onPress={() => updateServiceType(index)}
              key={index}
              style={[serviceAreaSingle, item.isActive && activeService]}>
              <Text style={[item.isActive ? {color: '#fff'} : {color: '#000'}]}>
                {item.name}
              </Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    </View>
  );
}
