import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from './styles';

export default function BasketArea(props) {
  const {
    navigation,
    totalPrice,
    restaurantInfo,
    restaurantId,
    restaurantUniId,
  } = props;

  const {viewBasketArea, viewBasketText} = styles;

  const onClickViewBasket = () => {
    navigation.navigate('AddToCart', {
      restaurantInfo,
      restaurantId,
      restaurantUniId,
    });
  };

  return (
    <TouchableOpacity
      style={viewBasketArea}
      onPress={() => onClickViewBasket()}>
      <Text style={viewBasketText}>View Basket</Text>
      <Text style={viewBasketText}>£{totalPrice}</Text>
    </TouchableOpacity>
  );
}
