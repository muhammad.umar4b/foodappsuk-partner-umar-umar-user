import moment from 'moment';
import React, {useContext, useState} from 'react';
import {Text, View} from 'react-native';
import {GlobalContext} from '../../context/GlobalContext';
import globalStyles from '../../styles/globalStyles';
import styles from './styles';

export default function Header({item}) {
  const {globalState, setGlobalState} = useContext(GlobalContext);
  const {restaurantId, restaurantUniId, postCode} = globalState;
  const [restaurant, setRestaurant] = useState(item.restaurant);

  const {restaurantHeader} = styles;

  const {f18, textCenter, fw700} = globalStyles;

  const {createdAt, orderNumber} = item;

  return (
    <View style={restaurantHeader}>
      <Text style={[f18, textCenter, fw700]}>{restaurantId}</Text>

      <Text style={textCenter}>{postCode}</Text>

      <Text style={textCenter}>{moment(createdAt).format('DD/MM/YYYY')}</Text>

      <Text style={textCenter}>{orderNumber}</Text>
    </View>
  );
}
