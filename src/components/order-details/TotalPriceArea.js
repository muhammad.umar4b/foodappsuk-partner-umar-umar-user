import React from 'react';
import {Text, View} from 'react-native';

import styles from './styles';
import globalStyles from '../../styles/globalStyles';

export default function TotalPriceArea({item}) {
  const {totalPriceArea} = styles;

  const {
    flexDirectionRow,
    justifyBetween,
    fw700,
    paddingLeft5,
    paddingBottom1,
    paddingRight7,
  } = globalStyles;

  const {paymentMethod, totalPrice, serviceCharge, deliveryCharge} = item;
  // console.log('first', item);

  return (
    <View style={totalPriceArea}>
      <View style={[flexDirectionRow, justifyBetween]}>
        <Text style={[fw700, paddingLeft5, paddingBottom1]}>
          Total Payment ({paymentMethod})
        </Text>
        <Text style={[fw700, paddingRight7]}>
          £
          {totalPrice
            ? parseFloat(totalPrice + serviceCharge).toFixed(2)
            : '0.00'}
        </Text>
      </View>
    </View>
  );
}
