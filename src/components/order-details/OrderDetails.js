import React, {useContext, useEffect, useLayoutEffect, useState} from 'react';
import {View, ScrollView, SafeAreaView, BackHandler} from 'react-native';
import {HeaderBackButton} from '@react-navigation/stack';
import {useFocusEffect} from '@react-navigation/native';

import Header from './Header';
import Body from './Body';
import TotalPriceArea from './TotalPriceArea';
import PriceArea from './PriceArea';
import Footer from './Footer';
import Loader from '../../utilities/components/Loader';

import styles from './styles';

import {GlobalContext} from '../../context/GlobalContext';
import {getTotalPrice} from '../../utilities/functions';

const OrderDetails = ({route, navigation}) => {
  const {item, serviceCharge} = route.params;
  const tableHead = ['Qty', 'Description', 'Amount'];
  const [tableData, setTableData] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const {globalState} = useContext(GlobalContext);
  const {customer} = globalState;

  const onBackPress = () => {
    if (item && item.tableNo) {
      navigation.navigate('Home');
    } else navigation.goBack();
    return true;
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: props => (
        <HeaderBackButton {...props} onPress={() => onBackPress()} />
      ),
    });
  }, [navigation]);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  useEffect(() => {
    let foodItems = JSON.parse(item.cartItems).foodItems;
    if (item) {
      let updateTableData = [];
      let totalAmount = 0;
      if (!Array.isArray(foodItems)) {
        const qty = parseInt(foodItems[0].quantity);
        const price = parseFloat(foodItems[0].price).toFixed(2);
        totalAmount += qty * price;
        const priceText = `£${(qty * price).toFixed(2)}`;
        // console.log('first', qty, price);
        const arr = [qty, foodItems.name, priceText];
        updateTableData.push(arr);
      }

      foodItems.forEach(item => {
        const qty = parseInt(item.quantity);
        const price = parseFloat(item.price).toFixed(2);
        totalAmount += qty * price;
        const priceText = `£${(qty * price).toFixed(2)}`;
        const arr = [qty, item.name, priceText];
        updateTableData.push(arr);
      });

      setTotalPrice(totalAmount.toFixed(2));
      setTableData(updateTableData);
    }

    if (item && item.tableNo) {
      item.totalPrice = getTotalPrice(foodItems);
    }
  }, [item]);

  console.log('Item', item);
  const {container} = styles;

  return item ? (
    <SafeAreaView>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={container}>
          {item.restaurant && <Header item={item} />}
          <Body tableHeadData={tableHead} tableData={tableData} />
          <PriceArea
            item={item}
            totalPrice={totalPrice}
            serviceCharge={serviceCharge}
          />
          <TotalPriceArea item={item} />
          <Footer item={item} customer={customer} />
        </View>
      </ScrollView>
    </SafeAreaView>
  ) : (
    <Loader />
  );
};

export default OrderDetails;
