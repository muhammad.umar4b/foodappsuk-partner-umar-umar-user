import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Pressable,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import globalStyles from '../styles/globalStyles';
import {GlobalContext} from '../context/GlobalContext';
import {showToastWithGravityAndOffset} from '../utilities/components/ToastMessage';
import axios from 'axios';
import {apiBaseUrl} from '../config';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import CheckBox from '@react-native-community/checkbox';

// Image
import cashImage from '../assets/image/cash.png';
import cardImage from '../assets/image/card.png';

const Payment = ({navigation, route}) => {
  const [restaurantInfo, setRestaurantInfo] = useState(null);
  const [cartList, setCartList] = useState([]);
  let [totalPrice, setTotalPrice] = useState(0);
  const [serviceCharges, setServiceCharges] = useState(0);
  const [restaurantData, setRestaurantData] = useState(0);
  const [deliveryCharge, setDeliveryCharge] = useState(0);
  const {serviceCharge} = serviceCharges;
  const [paymentType, setPaymentType] = useState([
    {
      id: 1,
      value: 'cash',
      name: 'Cash',
      img: cashImage,
      selected: false,
    },
    {
      id: 2,
      value: 'card',
      name: 'Card',
      img: cardImage,
      selected: false,
    },
    {
      id: 3,
      value: 'counter',
      name: 'Counter',
      img: null,
      icon: <FontAwesome5 name="cash-register" size={24} color="#524F4FFF" />,
      selected: false,
    },
  ]);
  const [isTermAndConditions, setTermAndConditions] = useState(false);
  const reSetCartList = {
    note: '',
    customer: '',
    restaurant: '',
    count: 0,
    subTotal: 0,
    foodItems: [],
  };
  const {globalState, setGlobalState} = useContext(GlobalContext);
  const {
    service_type,
    customer,
    kitchenNotes,
    cartList: globalCartList,
    sign_in_token,
    postCode,
    restaurantPostCode,
  } = globalState;

  const config = {
    headers: {
      'Content-type': 'application/json',
      Authorization: `Bearer ${sign_in_token}`,
    },
  };

  const {
    orderState: {customerName, address, phoneNumber, tableNo, distance},
    restaurantId,
    restaurantUniId,
  } = route.params;

  const payload11 = {
    postCode,
    restaurantPostCode: restaurantPostCode,
    restaurant: restaurantUniId,
  };

  const getServiceCharge = async () => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}service-charge/get-service-charge`,
      );

      if (response.data) {
        let {serviceCharge} = response.data.data;
        setGlobalState({
          ...globalState,
          ...{
            serviceCharge: serviceCharge,
          },
        });
        setServiceCharges(response.data.data);
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const getRestaurantById = async id => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}restaurant/get-restaurant-info/${id}`,
        {},
        config,
      );
      // setRestaurantData(response.data);
      setRestaurantInfo(response.data);
      return response.data;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const getRestaurantInfo = async () => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}restaurant/get-own-restaurant-info`,
        config,
      );
      if (response.data) {
        setRestaurantData(response.data.data);
      }
      return response.data.data;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
        // if (error.response.data.error)
        // showToastWithGravityAndOffset(error.response.data.error);
      }

      return null;
    }
  };

  const calculateDeliveryCharge = async payload11 => {
    try {
      const response = await axios.post(
        `${apiBaseUrl}restaurant-delivery/calculate-delivery-charge`,
        payload11,
      );

      if (response.data) {
        return response.data.data;
      } else return null;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
        if (error.response.data.error)
          showToastWithGravityAndOffset(error.response.data.error);
      }

      return null;
    }
  };

  const removeCart = async () => {
    const updateGlobalState = {...globalState};
    updateGlobalState.cartList = {
      note: '',
      customer: '',
      restaurant: '',
      count: 0,
      subTotal: 0,
      foodItems: [],
    };
    setGlobalState({...globalState, ...updateGlobalState});
  };

  const setRestaurantCart = data => {
    setCartList(data);
    return true;
  };

  useEffect(() => {
    if (restaurantId) {
      getRestaurantById(restaurantId);
      getServiceCharge();
      getRestaurantInfo().then(res => {
        calculateDeliveryCharge(payload11).then(res => {
          console.log('DELIVERY CHARGE: ', !!res);
          if (res) setDeliveryCharge(res);
        });
      });
    }
  }, []);

  useEffect(() => {
    console.log('Hello', cartList);
  }, [cartList]);

  useEffect(() => {
    if (restaurantId && customer && Object.keys(customer).length > 0) {
      setRestaurantCart(globalCartList);
    }
  }, [restaurantId, customer]);

  useEffect(() => {
    const updatePaymentType = paymentType.filter(item => {
      if (service_type === 'delivery') {
        if (['cash', 'card'].includes(item.value)) return item;
      }
      if (service_type === 'collection') {
        if (['cash', 'card'].includes(item.value)) return item;
      }
      if (service_type === 'dine_in') {
        if (['counter', 'card'].includes(item.value)) return item;
      }
    });
    setPaymentType(updatePaymentType);
  }, [service_type]);

  const onRadioBtnClick = item => {
    const updateState = paymentType.map(value =>
      item.id === value.id
        ? {...value, selected: true}
        : {...value, selected: false},
    );
    setPaymentType(updateState);
  };

  // console.log('service charge', serviceCharge);
  // useEffect(() => {
  //   // let totalPrice = 0;
  //   let {count} = cartList;
  //   totalPrice = cartList.subTotal;
  //   totalPrice = parseInt(count) * parseFloat(totalPrice);
  //   // const price = 1.25;
  //   // console.log('foodItems', foodItems);
  //   // if (!Array.isArray(foodItems)) {
  //   //   totalPrice += parseInt(count) * parseFloat(price);
  //   // }
  //   // foodItems.forEach(myFunction);
  //   // function myFunction(item) {
  //   //   totalPrice += parseInt(count) * parseFloat(item.price);
  //   // }

  //   // cartList.foodItems.forEach(
  //   //   item => (totalPrice += parseInt(item.count) * parseFloat(item.price)),
  //   // );
  //   console.log('totalPrice', totalPrice);
  //   setTotalPrice(parseFloat(totalPrice.toFixed(2)));
  // }, [cartList]);

  // let discount;
  // let deliveryCharge = 0;
  // let totalPayment;

  // if (restaurantInfo) {
  //   // console.log(totalPrice, 'totalPrice');
  //   discount = restaurantInfo.discount
  //     ? totalPrice * (parseInt(restaurantInfo.discount) / 100)
  //     : 0.0;
  //   if (service_type === 'delivery') {
  //     deliveryCharge = restaurantInfo.deliveryCharges
  //       ? parseFloat(restaurantInfo.deliveryCharges)
  //       : 0.0;
  //   }
  //   totalPayment = totalPrice;

  //   if (service_type === 'delivery') totalPayment += deliveryCharge;
  //   if (service_type !== 'dine_in') {
  //     // totalPayment -= discount;
  //     totalPayment += serviceCharge;
  //   }
  // }

  // console.log('totalPayment', totalPayment);
  totalPrice = cartList.subTotal;

  if (service_type === 'dine_in') {
    totalPrice = parseFloat(parseFloat(totalPrice + serviceCharge).toFixed(2));
  }
  if (service_type === 'collection') {
    totalPrice = parseFloat(parseFloat(totalPrice + serviceCharge).toFixed(2));
  }

  if (service_type === 'delivery') {
    console.log('Utility price', totalPrice);
    console.log('Utility delivery Price', deliveryCharge);
    totalPrice = parseFloat(
      (totalPrice + parseFloat(deliveryCharge.toString())).toFixed(2),
    );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

  const placeOrder = async (buttonType = '') => {
    const paymentOptions = paymentType.find(item => item.selected === true);

    let payload = {
      restaurant: restaurantUniId,
      restaurantUniId: restaurantId,
      customerName,
      customer: customer,
      mobileNo: cartList.mobileNo,
      subTotal: cartList.subTotal,
      totalPrice: totalPrice,
      paymentMethod: paymentOptions ? paymentOptions.value : '',
      kitchenNotes: cartList.note,
      cartItems: JSON.stringify(cartList),
      fromRestaurant: true,
    };

    switch (service_type) {
      case 'dine_in':
        payload.orderType = 'Dine In';
        payload.tableNo = parseInt(tableNo);
        payload.serviceCharge = serviceCharge;
        if (
          paymentOptions.value === 'Card' ||
          paymentOptions.value === 'card'
        ) {
          navigation.navigate('CardPayment', {orderPayload: payload});
          return false;
        }

        payload.paymentMethod = 'Counter';
        payload.paymentStatus = 'Pending';

        try {
          const dineInResponse = await axios.post(
            `${apiBaseUrl}order/create-customer-dine-in`,
            payload,
            config,
          );

          if (dineInResponse.data) {
            showToastWithGravityAndOffset('Order placed successfully!');
            removeCart();
            // console.log('Dine_In', dineInResponse.data.data);
            navigation.navigate('MyOrder', {
              orderDetails: dineInResponse.data.data,
              serviceCharge: serviceCharge,
            });
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
        }
        break;
      case 'collection':
        payload.orderType = 'Collection';
        payload.serviceCharge = serviceCharge;
        if (
          paymentOptions.value === 'Card' ||
          paymentOptions.value === 'card'
        ) {
          navigation.navigate('CardPayment', {orderPayload: payload});
          return false;
        }
        payload.paymentMethod = 'Counter';
        payload.paymentStatus = 'Pending';
        console.log('payload collection', payload);

        try {
          const collectionResponse = await axios.post(
            `${apiBaseUrl}order/create-customer-order`,
            payload,
            config,
          );

          if (collectionResponse.data) {
            // console.log('payload collection', payload);
            showToastWithGravityAndOffset('Order placed successfully!');
            removeCart();
            console.log(
              'Data after Collection Call',
              collectionResponse.data.data,
            );
            navigation.navigate('MyOrder', {
              serviceCharge: serviceCharge,
              isOrderPlaced: true,
            });
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
        }
        break;
      case 'delivery':
        payload.deliveredAt = address;
        payload.orderType = 'Home Delivery';
        payload.deliveryCharge = deliveryCharge;

        payload.totalPrice = totalPrice;

        if (
          paymentOptions.value === 'Card' ||
          paymentOptions.value === 'card'
        ) {
          navigation.navigate('CardPayment', {orderPayload: payload});
          return false;
        }
        payload.paymentMethod = 'Counter';
        payload.paymentStatus = 'Pending';

        try {
          const deliveryResponse = await axios.post(
            `${apiBaseUrl}order/create-customer-order`,
            payload,
            config,
          );

          if (deliveryResponse.data) {
            showToastWithGravityAndOffset('Order placed successfully!');
            console.log('delivery', deliveryResponse.data);

            removeCart();
            navigation.navigate('MyOrder', {isOrderPlaced: true});
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
        }
        break;
      default:
        return true;
    }

    return true;
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={[globalStyles.flex1, globalStyles.alignItemsBetween]}>
        <View
          style={[globalStyles.paddingHorizontal5, styles.paymentOptionArea]}>
          <View>
            <Text style={styles.paymentOptionLabel}>
              Choose a Payment Option
            </Text>
            <View style={globalStyles.radioButtonArea}>
              {paymentType.map(item => (
                <View style={globalStyles.radioButtonContainer} key={item.id}>
                  <TouchableOpacity
                    onPress={() => onRadioBtnClick(item)}
                    style={globalStyles.radioButton}>
                    {item.selected ? (
                      <View style={globalStyles.radioButtonIcon} />
                    ) : null}
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => onRadioBtnClick(item)}
                    style={globalStyles.radioButtonContentArea}>
                    {item.img && (
                      <Image
                        style={globalStyles.radioButtonImage}
                        source={item.img}
                      />
                    )}
                    {item.icon && (
                      <View style={globalStyles.marginRight1}>{item.icon}</View>
                    )}
                    <Text style={globalStyles.radioButtonText}>
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          </View>
          {restaurantInfo && (
            <View style={styles.orderSummaryArea}>
              <Text style={[globalStyles.f18]}>Your Order</Text>

              <View style={styles.cartListArea}>
                {cartList.foodItems.map((item, index) => (
                  <View
                    style={[
                      globalStyles.flexDirectionRow,
                      globalStyles.justifyBetween,
                    ]}
                    key={index}>
                    <Text>
                      {item.quantity} x {item.name}
                      {item.option ? ' - ' + item.option.name : ''}
                    </Text>
                    <Text>£{parseFloat(item.price).toFixed(2)}</Text>
                  </View>
                ))}
              </View>

              <View style={globalStyles.paddingTop05}>
                <View
                  style={[
                    globalStyles.flexDirectionRow,
                    globalStyles.justifyBetween,
                  ]}>
                  <Text>Sub Total</Text>
                  <Text>£{parseFloat(cartList.subTotal).toFixed(2)}</Text>
                </View>
                {(service_type === 'dine_in' ||
                  service_type === 'collection') && (
                  <View
                    style={[
                      globalStyles.flexDirectionRow,
                      globalStyles.justifyBetween,
                    ]}>
                    <Text>Service Charges (+)</Text>
                    <Text>£{serviceCharge}</Text>
                  </View>
                )}
              </View>

              {service_type === 'delivery' && (
                <View
                  style={[
                    globalStyles.flexDirectionRow,
                    globalStyles.justifyBetween,
                  ]}>
                  <Text>Delivery Charge (+)</Text>
                  <Text>
                    £{deliveryCharge ? deliveryCharge.toFixed(2) : '0.00'}
                  </Text>
                </View>
              )}

              {/* {service_type !== 'dine_in' && (
                <View
                  style={[
                    globalStyles.flexDirectionRow,
                    globalStyles.justifyBetween,
                  ]}>
                  <Text>Service Charges (+)</Text>
                  <Text>£{serviceCharge.toFixed(2)}</Text>
                </View>
              )} */}

              <View style={globalStyles.paddingTop05} />

              <View style={styles.totalPayment}>
                <Text>Total Payment</Text>
                <Text>
                  £{totalPrice ? totalPrice.toFixed(2) : '0.00'}
                  {/* {serviceCharge
                    ? (serviceCharge + cartList.subTotal).toFixed(2)
                    : cartList.subTotal.toFixed(2)} */}
                </Text>
              </View>

              <View style={globalStyles.paddingTop1}>
                <Text>Kitchen Notes : {kitchenNotes}</Text>
              </View>
            </View>
          )}
        </View>
        <View style={globalStyles.paddingHorizontal5}>
          <View style={globalStyles.flexDirectionRow}>
            <CheckBox
              value={isTermAndConditions}
              onValueChange={() => setTermAndConditions(!isTermAndConditions)}
              tintColors={{true: '#D2181B', false: 'black'}}
            />
            <Text style={globalStyles.paddingTop05}>
              Before your order Please make sure your food Allergy
              <Pressable
                onPress={() => navigation.navigate('TermsAndConditions')}>
                <Text style={globalStyles.textRed}>Terms and Conditions.</Text>
              </Pressable>
            </Text>
          </View>
          <View style={styles.continueButtonArea}>
            <TouchableOpacity
              style={[styles.continueButton]}
              onPress={() => placeOrder()}>
              <Text style={styles.continueText}>Proceed Check out</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  paymentOptionArea: {
    paddingVertical: hp('2%'),
  },
  paymentOptionLabel: {
    fontSize: 18,
    fontWeight: '700',
    color: '#D2181B',
  },
  continueButtonArea: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: hp('2%'),
    marginBottom: hp('8%'),
  },
  continueButton: {
    backgroundColor: '#D2181B',
    paddingVertical: hp('1.5%'),
    paddingHorizontal: wp('10%'),
    borderRadius: 8,
  },
  continueText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '700',
    textAlign: 'center',
  },
  orderSummaryArea: {
    borderWidth: 1,
    borderColor: '#b4b4b4',
    paddingHorizontal: wp('3%'),
    paddingVertical: hp('1%'),
    borderRadius: 8,
    marginTop: hp('1%'),
  },
  cartListArea: {
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
    paddingVertical: hp('0.5%'),
  },
  totalPayment: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderColor: '#b4b4b4',
    paddingTop: hp('0.5%'),
  },
});

export default Payment;
