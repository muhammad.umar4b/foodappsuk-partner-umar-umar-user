import {HeaderBackButton} from '@react-navigation/stack';
import axios from 'axios';
import React, {useContext, useEffect, useLayoutEffect, useState} from 'react';
import {
  Image,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import {apiBaseUrl} from '../config';
import {GlobalContext} from '../context/GlobalContext';
import globalStyles from '../styles/globalStyles';
import Loader from '../utilities/components/Loader';
import BasketArea from './BasketArea';
import ServiceTypeArea from './ServiceTypeArea';
import SpecialOfferModal from './special-offer-modal/SpecialOfferModal';

const RestaurantDetails = ({navigation, route}) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const [bookingModalVisible, setBookingModalVisible] = useState(false);
  const [modalState, setModalState] = useState(null);
  const [activeCategory, setActiveCategory] = useState(null);
  const [foodItemList, setFoodItemList] = useState([]);
  const [restaurantInfoLoading, setRestaurantInfoLoading] = useState(true);
  const [isSpecialOfferModalVisible, setIsSpecialOfferModalVisible] =
    useState(false);
  const [specialOfferModalState, setSpecialOfferModalState] = useState(null);
  const [serviceListArea, setServiceListArea] = useState([
    {
      key: 'delivery',
      name: 'Delivery',
      isActive: false,
      isEnabled: false,
    },
    {
      key: 'collection',
      name: 'Collection',
      isActive: true,
      isEnabled: false,
    },
    {
      key: 'dine_in',
      name: 'Dine In',
      isActive: false,
      isEnabled: false,
    },
  ]);
  const [categoryList, setCategoryList] = useState([]);
  const [isViewBasket, setIsViewBasket] = useState(false);
  const [state, setState] = useState({
    name: '',
    contact: '',
    bookingFor: '',
    guestNo: '',
    date: '',
    bookingFrom: '',
    count: 1,
    isAdd: false,
    twice: false,
    isRemove: false,
  });

  const {globalState, setGlobalState} = useContext(GlobalContext);

  const {cartList} = globalState;
  const {count, isAdd} = state;

  const [categoryId, setCategoryId] = useState('');
  const [restaurantInfo, setRestaurantInfo] = useState([]);
  const {restaurantId, restaurantUniId, restaurantPostCode} = route.params;

  const onBackPress = () => {
    navigation.navigate('Restaurant');
    const updateGlobalState = {...globalState};
    updateGlobalState.cartList = {
      note: '',
      customer: '',
      restaurant: '',
      count: 0,
      subTotal: 0,
      foodItems: [],
    };
    setGlobalState({...globalState, ...updateGlobalState});
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      title: restaurantInfo ? restaurantInfo.name : 'Restaurant',
      headerLeft: props => (
        <HeaderBackButton {...props} onPress={() => onBackPress()} />
      ),
    });
  }, [navigation, serviceListArea, restaurantInfo]);

  useEffect(() => {
    if (modalState && isAdd) {
      const {_id, name, options, basePrice, restaurant} = modalState;

      const foodItem = {
        food: _id,
        name,
        basePrice,
        options,
        optionsPrice: 0,
        price: parseFloat(parseFloat(basePrice * count).toFixed(2)),
        quantity: count,
        isAdded: false,
        uniqueId:
          name.substring(0, 3).toUpperCase() +
          Math.floor(Math.random() * 1000) +
          1,
      };

      cartList.foodItems.push(foodItem);
      let subTotal = 0;
      cartList.foodItems.forEach(item => {
        subTotal = parseFloat(parseFloat(subTotal + item.price).toFixed(2));
      });
      cartList.subTotal = subTotal;
      cartList.count = cartList.foodItems.length;
      cartList.restaurant = restaurant;

      setGlobalState({
        ...globalState,
        cartList,
        restaurantId: restaurantId,
        restaurantUniId: restaurantUniId,
        restaurantPostCode: restaurantPostCode,
      });
    }
  }, [count, modalState, isAdd]);

  // ///////////////////////////////////////////////////////
  const getRestaurantInfo = async id => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}restaurant/get-restaurant-info/${id}`,
      );
      setRestaurantInfo(response.data.data);
      return response.data;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const getRestaurantById = async id => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}category/get-restaurant-categories/${id}`,
      );
      setCategoryList(response.data?.data?.categories);
      setRestaurantInfo(response.data?.data);

      return response.data;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const getcategoryFoodById = async categoryId => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}food/get-foods-by-category/${categoryId}`,
      );

      setFoodItemList(response?.data?.data);
      return response.data;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  useEffect(() => {
    setRestaurantInfo(null);
    if (restaurantId) {
      getRestaurantById(restaurantId).then(res => {
        Object.keys(res.data).length > 0 && setRestaurantInfo(res.data);
        setRestaurantInfoLoading(false);
      });
    }
    getRestaurantInfo(restaurantId);
  }, [restaurantId, categoryId]);

  /////////////////////////////////////////////////////////////////////////////

  const toggleCategory = activeCategory => {
    const updateCategoryList = categoryList.map(item => {
      if (item._id === activeCategory) {
        item.isActive = !item.isActive;
      } else item.isActive = false;

      return item;
    });

    setCategoryList(updateCategoryList);
    setActiveCategory(activeCategory);
  };

  const toggleAddToCart = (foodItemList, index) => {
    const foodItem = foodItemList.find((item, key) => key === index);

    setState({
      count: 1,
      isAdd: false,
    });

    if (foodItem.options.length > 0) {
      setSpecialOfferModalState(foodItem);
      setIsSpecialOfferModalVisible(!isSpecialOfferModalVisible);
    } else {
      setModalState(foodItem);
      setModalVisible(!isModalVisible);
    }
    // setModalState(foodItem);
    // setModalVisible(!isModalVisible);
  };

  const onClickAddToCart = () => {
    if (count >= 1) {
      const updateState = {...state};
      updateState.count = count;
      updateState.isAdd = true;
      setState(updateState);
    }
    setModalVisible(!isModalVisible);
    setIsViewBasket(true);
    hideCategoryList();
  };
  const hideCategoryList = () => {
    const updateCategoryList = categoryList.map(item => {
      item.isActive = false;
      return item;
    });

    setCategoryList(updateCategoryList);
    setActiveCategory(null);
  };

  const updateServiceType = key => {
    const updateServiceListArea = serviceListArea.map((item, index) => {
      item.isActive = key === index;

      return item;
    });

    const activeService = updateServiceListArea.find(
      item => item.isActive === true,
    );
    const updateGlobalState = {...globalState};
    updateGlobalState.service_type = activeService.key;

    setGlobalState(updateGlobalState);
    setServiceListArea(updateServiceListArea);
  };

  const customHeight = isViewBasket ? {height: hp('70%')} : {height: hp('80%')};

  const renderRestaurantInfo = restaurantInfo &&
    Object.keys(restaurantInfo).length > 0 && (
      <>
        <View style={styles.restaurantInfoArea}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <ServiceTypeArea
              serviceListArea={serviceListArea}
              updateServiceType={updateServiceType}
            />
          </ScrollView>
        </View>
        <ScrollView>
          <View>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              style={[styles.categoryArea, customHeight]}
              pagingEnabled={true}>
              {categoryList.map((item, index) => (
                <View
                  style={[styles.categoryAreaSingle, styles.boxShadow]}
                  key={index}>
                  <Pressable
                    style={[
                      styles.categoryView,
                      item.isActive ? styles.categoryViewActive : {},
                    ]}
                    onPress={() => {
                      getcategoryFoodById(item._id);
                      toggleCategory(item._id);
                    }}>
                    <Text
                      style={item.isActive ? {color: '#fff'} : {color: '#000'}}>
                      {item.name}
                    </Text>
                    <AntDesign
                      name={item.isActive ? 'downcircleo' : 'upcircleo'}
                      size={22}
                      color={item.isActive ? '#fff' : '#000'}
                    />
                  </Pressable>
                  {item.isActive &&
                    foodItemList.map((item, index) => (
                      <TouchableOpacity
                        style={styles.detailsArea}
                        key={index}
                        onPress={() => toggleAddToCart(foodItemList, index)}>
                        <View style={styles.detailsAreaLeft}>
                          <Text style={styles.detailsAreaTitle}>
                            {item.name}
                          </Text>
                          {item.description !== '' && (
                            <Text style={styles.detailsAreaDescription}>
                              {item.description}
                            </Text>
                          )}
                          <Text style={styles.detailsAreaAmount}>
                            £{item.price}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    ))}
                </View>
              ))}
            </ScrollView>
          </View>

          <SpecialOfferModal
            cartList={cartList}
            setIsViewBasket={setIsViewBasket}
            specialOfferModalState={specialOfferModalState}
            isSpecialOfferModalVisible={isSpecialOfferModalVisible}
            setIsSpecialOfferModalVisible={setIsSpecialOfferModalVisible}
            hideCategoryList={hideCategoryList}
          />

          {/* {isViewBasket && (
          <TouchableOpacity
            style={styles.viewBasketArea}
            onPress={() => navigation.navigate('AddToCart')}
          >
            <Text style={styles.viewBasketText}>View Basket</Text>
            <Text style={styles.viewBasketText}>
              {modalState.price * count}
            </Text>
          </TouchableOpacity>
        )} */}
          {isViewBasket && (
            <BasketArea
              navigation={navigation}
              restaurantUniId={restaurantUniId}
              restaurantId={restaurantId}
              restaurantInfo={restaurantInfo}
              setRestaurantInfo={setRestaurantInfo}
              totalPrice={cartList ? cartList.subTotal : 0}
            />
          )}
          <Modal
            isVisible={isModalVisible}
            animationIn="fadeIn"
            animationOut="fadeOut"
            animationInTiming={500}
            animationOutTiming={500}
            style={globalStyles.modalView}>
            {modalState ? (
              <View>
                <ScrollView>
                  <View style={styles.modalHeaderArea}>
                    <Text style={styles.modalHeaderField}>
                      {modalState.name}
                    </Text>
                    <TouchableOpacity
                      style={styles.modalCloseIconArea}
                      onPress={() => setModalVisible(!isModalVisible)}>
                      <AntDesign name="close" size={18} color="#fff" />
                    </TouchableOpacity>
                  </View>
                  <View style={styles.modalBody}>
                    {modalState.images.length > 0 && (
                      <Swiper
                        style={globalStyles.height25p}
                        showsButtons={true}
                        showsPagination={false}
                        prevButton={<Text style={styles.swiperButton}>‹</Text>}
                        nextButton={<Text style={styles.swiperButton}>›</Text>}>
                        {modalState.images.map((item, index) => (
                          <View style={styles.modalImageArea} key={index}>
                            <Image style={styles.modalImage} source={item} />
                          </View>
                        ))}
                      </Swiper>
                    )}
                    {modalState.description !== '' && (
                      <Text style={styles.modalContentField}>
                        {modalState.description}
                      </Text>
                    )}
                    {foodItemList.map((item, index) => (
                      <View
                        style={[
                          globalStyles.flexDirectionRow,
                          globalStyles.justifyBetween,
                        ]}
                        key={item._id}></View>
                    ))}
                    <View
                      style={[
                        globalStyles.flexDirectionRow,
                        globalStyles.justifyCenter,
                      ]}>
                      <View style={styles.addToArea}>
                        <TouchableOpacity
                          onPress={() =>
                            count > 1 && setState({...state, count: count - 1})
                          }>
                          <Feather
                            name="minus-circle"
                            size={24}
                            color="#D2181B"
                          />
                        </TouchableOpacity>
                        <Text style={globalStyles.f18}>{count}</Text>
                        <TouchableOpacity
                          onPress={() =>
                            setState({...state, count: count + 1})
                          }>
                          <Feather
                            name="plus-circle"
                            size={24}
                            color="#D2181B"
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <TouchableOpacity
                      style={styles.addToButtonArea}
                      onPress={() => onClickAddToCart()}>
                      <Text style={styles.addToButton}>
                        Add For £{' '}
                        {parseFloat(
                          parseFloat(modalState.price) * count,
                        ).toFixed(2)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
              </View>
            ) : (
              <View>
                <Text>Not found!</Text>
              </View>
            )}
          </Modal>
        </ScrollView>
      </>
    );

  return restaurantInfoLoading ? <Loader /> : renderRestaurantInfo;
};

const styles = StyleSheet.create({
  // categoryArea: {
  //   flexDirection: 'row',
  // },
  // categoryAreaSingle: {
  //   paddingHorizontal: wp('5%'),
  //   paddingVertical: hp('0.5%'),
  //   marginRight: wp('1%'),
  //   borderRadius: 15,
  //   backgroundColor: '#fff',
  //   flexDirection: 'row',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   marginTop: hp('0.5%'),
  //   marginBottom: hp('1.5%'),
  // },
  categoryAreaImage: {
    width: wp('12.5%'),
  },
  dataHeaderText: {
    fontSize: 20,
    fontWeight: '700',
    paddingBottom: hp('1.5%'),
  },
  detailsArea: {
    paddingVertical: hp('1.5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopColor: '#dcd6d6',
    borderTopWidth: 1,
  },
  detailsAreaLeft: {
    width: wp('70%'),
  },
  detailsAreaTitle: {
    fontSize: 16,
    color: '#2e3333',
  },
  detailsAreaDescription: {
    fontSize: 14,
    color: '#828585',
    paddingTop: hp('0.5%'),
  },
  detailsAreaAmount: {
    fontSize: 14,
    color: '#828585',
    paddingTop: hp('0.5%'),
  },
  detailsAreaRight: {
    width: wp('20%'),
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  detailsAreaRightImage: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  modalHeaderArea: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
    paddingHorizontal: wp('4%'),
    paddingVertical: hp('2%'),
    borderBottomWidth: 1,
    borderBottomColor: '#CDC9C9',
  },
  modalHeaderField: {
    fontSize: wp('5%'),
    color: '#000',
    fontWeight: '600',
  },
  modalCloseIconArea: {
    backgroundColor: '#D2181B',
    width: 30,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  modalBody: {
    backgroundColor: '#fff',
    paddingHorizontal: wp('4%'),
    paddingBottom: hp('3.5%'),
  },
  modalContentField: {
    textAlign: 'justify',
    lineHeight: 23,
    fontSize: 16,
    color: '#000000',
    paddingTop: hp('1%'),
  },
  addToArea: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: hp('4%'),
    width: wp('50%'),
  },
  addToButtonArea: {
    marginTop: hp('8%'),
    borderRadius: 3,
    backgroundColor: '#D2181B',
    paddingVertical: hp('2%'),
  },
  swiperButton: {
    fontSize: 50,
    color: '#D2181B',
  },
  modalImageArea: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalImage: {
    width: 180,
    height: 180,
    borderRadius: 100,
  },
  addToButton: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  viewBasketArea: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: hp('2%'),
    paddingHorizontal: wp('5%'),
    elevation: 5,
    backgroundColor: '#D2181B',
  },
  viewBasketText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '600',
  },
  addonArea: {
    paddingTop: hp('3%'),
    paddingBottom: hp('1%'),
    borderBottomColor: '#CDC9C9',
    borderBottomWidth: 1,
    marginBottom: hp('0.5%'),
  },
  addonCheckBox: {
    borderColor: 'transparent',
    paddingVertical: hp('0%'),
    paddingHorizontal: wp('0%'),
    backgroundColor: '#fff',
  },
  restaurantInfoArea: {
    paddingHorizontal: wp('5%'),
    paddingTop: hp('1%'),
  },
  favouriteAreaImage: {
    width: wp('100%'),
    height: hp('15%'),
    position: 'relative',
  },
  restaurantInfoName: {
    fontSize: 18,
  },
  restaurantInfoAddress: {
    fontSize: 14,
    color: '#828585',
  },
  restaurantInfoHeader: {
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
  },
  deliveryTimeArea: {
    flexDirection: 'row',
    paddingTop: hp('1%'),
    paddingBottom: hp('0.5%'),
  },
  deliveryTimeAreaText: {
    paddingTop: hp('0.2%'),
    paddingLeft: wp('0.5%'),
    paddingRight: wp('1%'),
  },
  minimumOrder: {
    flexDirection: 'row',
    margin: hp('0.5%'),
  },
  serviceTypeArea: {
    paddingTop: hp('0.5%'),
    paddingBottom: hp('1%'),
  },
  serviceArea: {
    paddingTop: hp('0.5%'),
    paddingBottom: hp('1%'),
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
  },
  serviceAreaSingle: {
    paddingHorizontal: wp('3.5%'),
    paddingVertical: hp('0.5%'),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  activeService: {
    backgroundColor: '#666363',
    borderRadius: 15,
  },
  bookingButtonArea: {
    paddingTop: hp('2%'),
    width: wp('90%'),
  },
  bookingButton: {
    backgroundColor: '#D2181B',
    paddingVertical: hp('1.5%'),
    paddingHorizontal: wp('10%'),
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  bookingText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '700',
    paddingLeft: wp('2%'),
  },
  bookingIconImage: {
    width: 30,
    height: 25,
  },
  menuBarServiceArea: {
    paddingHorizontal: wp('4%'),
    paddingVertical: hp('0.5%'),
    borderRadius: 15,
    backgroundColor: '#D2181B',
    marginRight: wp('5%'),
  },
  inputField: {
    paddingVertical: hp('1%'),
    paddingLeft: wp('4%'),
    borderColor: '#d9d3d3',
    borderWidth: 1,
    borderRadius: 8,
  },
  inputLabel: {
    color: '#555555',
  },
  continueButtonArea: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: hp('3%'),
  },
  continueButton: {
    backgroundColor: '#D2181B',
    paddingVertical: hp('1.5%'),
    paddingHorizontal: wp('10%'),
    borderRadius: 8,
  },
  continueText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '700',
    textAlign: 'center',
  },
  dateTimeButton: {
    backgroundColor: '#27C96D',
    paddingVertical: hp('1.5%'),
    paddingHorizontal: wp('5%'),
    borderRadius: 8,
    width: wp('35%'),
  },
  dateTimeArea: {
    flexDirection: 'row',
  },
  dateTimeInput: {
    paddingVertical: hp('1%'),
    paddingLeft: wp('4%'),
    width: wp('82%'),
    borderColor: '#d9d3d3',
    borderWidth: 1,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  dateTimeIcon: {
    paddingVertical: hp('1.5%'),
    width: wp('10%'),
    textAlign: 'center',
    backgroundColor: '#D2181B',
    color: '#fff',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
  },
  selectionInputArea: {
    borderColor: '#d9d3d3',
    borderWidth: 1,
    borderRadius: 8,
  },
  logoArea: {
    position: 'absolute',
    top: hp('7%'),
    left: wp('5%'),
  },
  logoImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 3,
    borderColor: '#fff',
  },

  // categoryArea: {
  //   flex: 1,
  // },
  // categoryAreaSingle: {
  //   width: '100%',
  //   paddingHorizontal: wp('5%'),
  //   flexWrap: 'wrap',
  //   paddingVertical: hp('0.5%'),
  //   marginRight: wp('2%'),
  //   // borderRadius: 15,
  //   backgroundColor: '#fff',
  //   // flexDirection: 'column',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   marginVertical: hp('1.5%'),
  // },
  categoryAreaImage: {
    width: wp('12.5%'),
  },
  dataHeaderText: {
    fontSize: 20,
    fontWeight: '700',
    paddingBottom: hp('1.5%'),
  },
  detailsArea: {
    paddingVertical: hp('1.5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopColor: '#dcd6d6',
    borderTopWidth: 1,
  },
  detailsAreaLeft: {
    width: wp('70%'),
  },
  detailsAreaTitle: {
    fontSize: 16,
    color: '#2e3333',
  },
  detailsAreaDescription: {
    fontSize: 14,
    color: '#828585',
    paddingTop: hp('0.5%'),
  },
  detailsAreaAmount: {
    fontSize: 14,
    color: '#828585',
    paddingTop: hp('0.5%'),
  },
  detailsAreaRight: {
    width: wp('20%'),
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  detailsAreaRightImage: {
    width: 70,
    height: 70,
    borderRadius: 35,
  },
  modalHeaderArea: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
    paddingHorizontal: wp('4%'),
    paddingVertical: hp('2%'),
    borderBottomWidth: 1,
    borderBottomColor: '#CDC9C9',
  },
  modalHeaderField: {
    fontSize: wp('5%'),
    color: '#000',
    fontWeight: '600',
  },
  modalCloseIconArea: {
    backgroundColor: '#D2181B',
    width: 30,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3,
  },
  modalBody: {
    backgroundColor: '#fff',
    paddingHorizontal: wp('4%'),
    paddingTop: hp('2%'),
    paddingBottom: hp('3.5%'),
  },
  modalContentField: {
    textAlign: 'justify',
    lineHeight: 23,
    fontSize: 16,
    color: '#000000',
    paddingTop: hp('1%'),
  },
  addToArea: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: hp('4%'),
    width: wp('50%'),
  },
  addToButtonArea: {
    marginTop: hp('8%'),
    borderRadius: 3,
    backgroundColor: '#D2181B',
    paddingVertical: hp('2%'),
  },
  swiperButton: {
    fontSize: 50,
    color: '#D2181B',
  },
  modalImageArea: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalImage: {
    width: 180,
    height: 180,
    borderRadius: 100,
  },
  addToButton: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  viewBasketArea: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: hp('2%'),
    elevation: 5,
    backgroundColor: '#D2181B',
  },
  viewBasketText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '600',
  },
  addonArea: {
    paddingTop: hp('3%'),
    paddingBottom: hp('1%'),
    borderBottomColor: '#CDC9C9',
    borderBottomWidth: 1,
    marginBottom: hp('0.5%'),
  },
  addonCheckBox: {
    borderColor: 'transparent',
    paddingVertical: hp('0%'),
    paddingHorizontal: wp('0%'),
    backgroundColor: '#fff',
  },
  restaurantInfoArea: {
    paddingHorizontal: wp('5%'),
    paddingTop: hp('1%'),
  },
  favouriteAreaImage: {
    width: wp('100%'),
    height: hp('26%'),
  },
  restaurantInfoName: {
    fontSize: 18,
  },
  restaurantInfoAddress: {
    fontSize: 14,
    color: '#828585',
  },
  restaurantInfoHeader: {
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
  },
  deliveryTimeArea: {
    flexDirection: 'row',
    paddingTop: hp('0.5%'),
    paddingBottom: hp('1%'),
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
  },
  deliveryTimeAreaLeft: {
    flexDirection: 'row',
  },
  deliveryTimeAreaText: {
    paddingTop: hp('0.2%'),
    paddingLeft: wp('1%'),
    color: '#D2181B',
  },
  deliveryTimeAreaRight: {
    flexDirection: 'row',
    paddingLeft: wp('5%'),
  },
  bookingButtonArea: {
    paddingTop: hp('2%'),
    width: wp('90%'),
  },
  bookingButton: {
    backgroundColor: '#D2181B',
    paddingVertical: hp('1.5%'),
    paddingHorizontal: wp('10%'),
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  bookingText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '700',
    paddingLeft: wp('2%'),
  },
  bookingIconImage: {
    width: 30,
    height: 25,
  },
  categoryArea: {
    marginHorizontal: wp('3%'),
    marginTop: hp('1%'),
  },
  categoryHeaderText: {
    marginHorizontal: wp('2%'),
    marginBottom: hp('0.5%'),
    fontWeight: '700',
  },
  boxShadow: {
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  categoryAreaSingle: {
    borderRadius: 5,
    borderBottomColor: '#b4b4b4',
    borderBottomWidth: 1,
    marginBottom: hp('0.1%'),
    // marginHorizontal: wp('0.5%'),
  },
  categoryView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp('94%'),
    paddingHorizontal: wp('3%'),
    paddingVertical: hp('5%'),
    borderRadius: 5,
  },
  categoryViewActive: {
    backgroundColor: '#D2181B',
    borderBottomStartRadius: 0,
    borderBottomEndRadius: 0,
  },
  detailsArea: {
    paddingVertical: hp('5%'),
    paddingHorizontal: hp('2%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopColor: '#dcd6d6',
    borderTopWidth: 1,
  },
  detailsAreaLeft: {
    width: wp('70%'),
  },
  detailsAreaTitle: {
    fontSize: 18,
    color: '#2e3333',
    fontWeight: '700',
  },
  detailsAreaDescription: {
    fontSize: 14,
    color: '#828585',
    paddingTop: hp('0.5%'),
  },
  detailsAreaAmount: {
    fontSize: 14,
    color: '#828585',
    paddingTop: hp('0.5%'),
  },
});

export default RestaurantDetails;
