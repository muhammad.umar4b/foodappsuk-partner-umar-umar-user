import React from 'react';

import {CheckBox} from 'react-native-elements';
import {Cell, TableWrapper} from 'react-native-table-component';

import styles from '../styles';
import globalStyles from '../../../assets/styles/globalStyles';

export default function TableBody(props) {
  const {tableIndex, tableBodyData, calculateSpecialOfferTotalPrice} = props;

  const {tableBody, tableText, tableTextRight} = styles;

  const {borderWhite, flexDirectionRow, justifyEnd} = globalStyles;

  const checkBoxButton = (cellData, rowIndex) => (
    <CheckBox
      containerStyle={[borderWhite, flexDirectionRow, justifyEnd]}
      checkedColor={'#d2181b'}
      onPress={() =>
        calculateSpecialOfferTotalPrice(!cellData, tableIndex, rowIndex)
      }
      checked={cellData}
    />
  );

  return tableBodyData.map((rowData, rowIndex) => (
    <TableWrapper key={rowIndex} style={tableBody}>
      {rowData.map((cellData, cellIndex) => (
        <Cell
          key={cellIndex}
          data={cellIndex === 2 ? checkBoxButton(cellData, rowIndex) : cellData}
          textStyle={[tableText, cellIndex === 2 && tableTextRight]}
        />
      ))}
    </TableWrapper>
  ));
}
