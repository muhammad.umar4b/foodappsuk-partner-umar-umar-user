import React from 'react';
import {View} from 'react-native';

import globalStyles from '../../../assets/styles/globalStyles';

import TableHead from './TableHead';
import TableBody from './TableBody';

const SpecialOfferTable = props => {
  const {item, tableIndex, calculateSpecialOfferTotalPrice} = props;

  const {paddingTop2} = globalStyles;

  const {tableHeadData, tableBodyData} = item;

  return (
    <View style={paddingTop2}>
      <TableHead tableHeadData={tableHeadData} />
      <TableBody
        tableBodyData={tableBodyData}
        tableIndex={tableIndex}
        calculateSpecialOfferTotalPrice={calculateSpecialOfferTotalPrice}
      />
    </View>
  );
};

export default SpecialOfferTable;
