import React, {useEffect} from "react";
import {Text} from "react-native";

import styles from "../styles";

import SpecialOfferTable from "./table/SpecialOfferTable";

export default function Body(props) {
    const {
        tableData,
        setTableData,
        totalPrice,
        setTotalPrice,
        specialOfferModalState,
    } = props;

    const {
        options,
        basePrice,
    } = specialOfferModalState;

    useEffect(() => {
        const updateTableData = options.map(item => {
            let tableBodyData = [];

            item.types.forEach(value => {
                tableBodyData.push([
                    value.typeName,
                    `(+) £${value.price}`,
                    false,
                ]);
            });

            return {
                tableHeadData: [
                    item.name,
                    `Choose: ${item["choiceCount"]}`,
                    `Selected: ${0}`,
                ],
                tableBodyData,
                choiceCount: parseInt(item["choiceCount"]),
                selected: 0,
            };
        });

        setTotalPrice(basePrice);
        setTableData(updateTableData);
    }, [specialOfferModalState]);

    const calculateSpecialOfferTotalPrice = (value, tableIndex, rowIndex) => {
        let updateTableData = [...tableData];
        let updateTotalPrice = totalPrice;

        updateTableData = updateTableData.map((tableItem, tableKey) => {
            if (tableKey === tableIndex) {
                const {
                    selected,
                    choiceCount,
                } = tableItem;

                if ((selected < choiceCount) && value) {
                    tableItem.selected += 1;
                    tableItem.tableHeadData[2] = `Selected: ${tableItem.selected}`;

                    tableItem.tableBodyData = tableItem.tableBodyData.map((rowItem, rowKey) => {
                        if (rowKey === rowIndex) {
                            rowItem[2] = value;
                            const optionPrice = parseFloat(parseFloat(rowItem[1].substring(5)).toFixed(2));
                            updateTotalPrice = parseFloat(parseFloat(updateTotalPrice + optionPrice).toFixed(2));
                        }

                        return rowItem;
                    });
                } else if ((selected === choiceCount) && value) {
                    alert(`Already selected ${choiceCount} option${choiceCount > 1 ? "s" : ""}!`);
                } else if (!value) {
                    tableItem.selected -= 1;
                    tableItem.tableHeadData[2] = `Selected: ${tableItem.selected}`;

                    tableItem.tableBodyData = tableItem.tableBodyData.map((rowItem, rowKey) => {
                        if (rowKey === rowIndex) {
                            rowItem[2] = value;
                            const optionPrice = parseFloat(parseFloat(rowItem[1].substring(5)).toFixed(2));
                            updateTotalPrice = parseFloat(parseFloat(updateTotalPrice - optionPrice).toFixed(2));
                        }

                        return rowItem;
                    });
                }
            }

            return tableItem;
        });

        setTotalPrice(updateTotalPrice);
        setTableData(updateTableData);
    };

    const {
        modalContentField,
    } = styles;

    const {
        description,
    } = specialOfferModalState;

    return (
        <>
            {description !== "" &&
                <Text style={modalContentField}>{description}</Text>
            }

            {tableData.map((item, index) => (
                <SpecialOfferTable
                    key={index}
                    tableIndex={index}
                    item={item}
                    calculateSpecialOfferTotalPrice={calculateSpecialOfferTotalPrice}
                />
            ))}
        </>
    );
}
